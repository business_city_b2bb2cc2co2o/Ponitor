# Pomitor

[ P ] rice + M [ onitor ] = Ponitor 价格监测

可添加天猫、淘宝、京东、Apple App的商品，监控商品价格发生变动时（每天定时器），推送消息（目前是发送邮件）！

技术栈：vue.js + ES6 + node.js + mongodb

[BAE在线体验](http://ponitor.duapp.com/)（提供已注册账号：ponitor，密码：ponitor）


# Usage


1、确保node版本（4.0以上）支持ES6

2、将`src/config.global.default.js`文件重命名为`config.global.js`

3、

- `npm install`

- `gulp`



# Screenshot

![index](https://raw.githubusercontent.com/giscafer/Ponitor/master/wiki/index_preview.png)

商品列表管理

![index](https://raw.githubusercontent.com/giscafer/Ponitor/master/wiki/goodlist_preview.png)

关注的商品价格发生变化（涨价or降价）的时候发生邮件通知：

![index](https://raw.githubusercontent.com/giscafer/Ponitor/master/wiki/email-sample2.png)

# Other

[wiki](https://github.com/giscafer/Ponitor/wiki)


# License

MIT
